from fuzzywuzzy import process

from api import products_data, headphone_data


def fzf_headphone(search, products=None):
    """fzf_headphone returns the graph data if is has an entry on rting, None otherwise.
    Also the products return is the products list for make use as cache

    Parameters
    ----------
    search: str
        query string of the headphone name
    products(optional): list[dict]
        cache for not repeat the product data requests

    Returns
    -------
    dict
        graph points data from the search query
    list[dict]
        product list for cache

    """

    if not products:
        products = products_data()
        if not products:
            exit("Fail while retrieving data!")

    candidates = process.extract(search, products)

    # TODO: make user choice between their desire result
    id, fullname = candidates[0][0]["id"], candidates[0][0]["fullname"]
    data = headphone_data(id)

    if not data:
        exit(f"Some error was found while fetching {id}!")

    del data["options"]

    return data, products


if __name__ == "__main__":

    from pprint import pprint

    products_cache = None
    while True:
        search = input("What headphone are you searching for?\n")
        if not search or search == "exit":
            break
        data, products_cache = fzf_headphone(search, products=products_cache)

        pprint(data)
