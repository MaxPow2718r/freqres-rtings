import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt


def parse_data_to_dataframe(data: dict) -> pd.core.frame.DataFrame:
    """parse_data_to_dataframe returns a dataframe of the given data.

    Parameters
    ----------
    data: dict
        data fetched from https://rtings.com/

    Returns
    -------
    DataFrame
        Pandas dataframe of the given data with columns; "Frequency",
        "Target Response", "Left hear" and "Right hear".

    """

    df = pd.DataFrame(data["data"], columns=data["header"], dtype="float")

    if type(df.Left) == pd.core.frame.DataFrame:
        lefts = list()
        for _, vals in df.Left.iterrows():
            if (sum := vals.sum()) > 100:
                sum /= 2
            lefts.append(sum)
        df["Left hear"] = pd.Series(lefts)
    elif type(df.Left) == pd.core.series.Series:
        df["Left hear"] = df.Left

    if type(df.Right) == pd.core.frame.DataFrame:
        rights = list()
        for _, vals in df.Right.iterrows():
            if (sum := vals.sum()) > 100:
                sum /= 2
            rights.append(sum)
        df["Right hear"] = pd.Series(rights)
    elif type(df.Right) == pd.core.series.Series:
        df["Right hear"] = df.Right

    df.drop(["Left", "Right"], axis=1, inplace=True)

    return df


def show_graph(df: pd.core.frame.DataFrame):

    sns.lineplot(x="Frequency", y="Target Response", data=df)
    sns.lineplot(x="Frequency", y="Left hear", data=df)
    sns.lineplot(x="Frequency", y="Right hear", data=df)
    # TODO: add log scale to x axis on plot
    plt.show()


if __name__ == "__main__":

    from fzf import fzf_headphone

    products_cache = None
    while True:
        search = input("What headphone are you searching for?\n")
        if not search:
            break
        data, products_cache = fzf_headphone(search, products=products_cache)
        df = parse_data_to_dataframe(data)

        show_graph(df)
