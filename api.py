import httpx

from secrets import headers


# TODO: clean data
def products_data() -> list[dict]:
    """products_data returns all the products with his related info from the
    table_tool_products_list/ endpoint of the rtings api v2

    Returns
    -------
    list[dict]
        list of headphones with review on rtings.com

    """

    # That data just works, copy from curl command
    url = "https://www.rtings.com/api/v2/safe/table_tool__products_list"
    data = '{"variables":{"test_bench_ids":["76","90"],"named_version":"public","is_admin":false}}'

    response = httpx.post(url, data=data, headers=headers)
    if response.status_code != 200:
        return None

    products = response.json()
    products = products["data"]["products"]

    return products


def headphone_data(id: int) -> dict:
    """headphone_data fetch by id the Raw Frequency Response (RFR) and returns
    a dict object with the points of the graph

    Parameters
    ----------
    id: int
        ID of product from /table_tool_products_list api endpoint

    Returns
    -------
    dict
        Points for graph of RFR
    """

    # 7903 -> Raw Frequency Response endpoint
    url = f"https://www.rtings.com/graph/data/{str(id)}/7903"

    response = httpx.get(url, headers=headers)

    # TODO: use pandas for better formating
    if response.status_code != 200:
        return None
    return response.json()


if __name__ == "__main__":

    # Products fetch
    products = products_data()
    print(len(products), "entries in https://rtings.com/")

    # Product data fetch by id
    id = 1579
    data = headphone_data(id)
    print(f"Size of {id} data:", len(data["data"]), "points")
